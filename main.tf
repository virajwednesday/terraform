provider "aws" {
    region = "eu-west-3"
}

resource "aws_vpc" "development-vpc"{
    cidr_block = "10.0.0.0/16"
    tags = {
        Name: "development"
    
    }

}

resource "aws_subnet" "dev-subnet-1"{
    vpc_id = aws_vpc.development-vpc.id 
    cidr_block = "10.0.10.0/24"
    availability_zone = "eu-west-3a"
    tags = {
        Name: "subnet-development-1"
    }
}

data "aws_vpc" "existing_vpc"{
    default = true
}

variable "subnet_cidr_block" {
  description = "subnet cidr value"
  type = object(
      {
          cidr_block = string
          name = string
      }
  )
}

variable "avail_zone" {}

resource "aws_subnet" "dev-subnet-2"{
    vpc_id = data.aws_vpc.existing_vpc.id
    cidr_block = var.subnet_cidr_block.cidr_block
    availability_zone = var.avail_zone
    tags = {
        Name: var.subnet_cidr_block.name
    }
}

output "dev-vpc-id"{
    value = aws_vpc.development-vpc.id
}







